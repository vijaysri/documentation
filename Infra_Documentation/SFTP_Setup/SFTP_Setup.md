# Setting up SFTP server to work with DRISTi

## Context 
DRISTi uses SFTP to upload images captured via Fundus cameras. These images are stored on an SFTP server.
If you're reading this, you must be trying to setup an SFTP server. Read on for the instructions.

## Steps

* Ensure that vsftp service is installed and running. For Ubuntu, that would be 
`sudo apt-get -y install vsftpd`

* vsftpd comes with a default configuration file. This **won't** work for us. 
As a good measure, backup the existing .conf file before following the next step. 

* In this repository, get the file called "vsftpd.conf" and place it at /etc/vsftpd.conf .
The important fields that might need changing are `local_root` and 
`rsa_cert_file=/etc/ssl/private/vsftpd.pem`
`rsa_private_key_file=/etc/ssl/private/vsftpd.pem`


>> **Important** : By default the .pem file will not exist. You need to generate one for yourself on the machine. 
Use the command `sudo openssl req -x509 -nodes -newkey rsa:1024 -keyout /etc/ssl/private/vsftpd.pem -out /etc/ssl/private/vsftpd.pem` to generate the same. OpenSSL will prompt for a few values. Ensure that you give the right ones. If you don't know some details, press enter. 

* The last step is to ensure that the SSH daemon on the machine can accept password based authentication. To do this, comment the line `PasswordAuthentication no`. In essence, the line should look like `#PasswordAuthentication no`

* Run the following commands : 

>> `sudo service sshd restart`

>> `sudo service vsftpd restart`

* Try logging in using `sftp` command on Linux or if you have Putty installed, try with `psftp`. You should be able to login using *only* the username and password. 