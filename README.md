# Documentation

Repository to track the documentation inside Artelus. Create separate folders for different areas . E.g. : Infrastructure folder to keep track of setup documents , Software folder to keep track of design documents etc.